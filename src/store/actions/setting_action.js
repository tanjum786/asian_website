import api from "../../domain/api";
import {
  GET_SETTINGS_STATED,
  GET_SETTINGS,
  GET_SETTINGS_ENDED,
  ADD_SETTING_STATED,
  ADD_SETTING,
  ADD_SETTING_ENDED,
  EDIT_SETTING_STATED,
  EDIT_SETTING,
  EDIT_SETTING_ENDED,
  GET_SETTING_STATED,
  GET_SETTING,
  GET_SETTING_ENDED,
  GET_ALL_SETTINGS_STATED,
  GET_ALL_SETTINGS,
  GET_ALL_SETTINGS_ENDED,
} from "../types/setting_type";
import * as qs from "qs";
import { handleError } from "../../shared/handleError";
import { setAlert } from "./alert";

export const addSetting = (formData) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ADD_SETTING_STATED,
    });
    const { data } = await api.post(`/settings`, formData);
    dispatch({
      type: ADD_SETTING,
      payload: data,
    });
    dispatch({
      type: ADD_SETTING_ENDED,
    });
  } catch (error) {
    dispatch({
      type: ADD_SETTING_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getSettings =
  ({ pageNumber = ""}) =>
  async (dispatch) => {
    try {
      dispatch({
        type: GET_SETTINGS_STATED,
      });
      const queryParams = qs.parse(window.location.search.replace("?", ""));
      const query = qs.stringify(queryParams, {
        encodeValuesOnly: true, // prettify url
      });

      const { data } = await api.get(
        `/settings?&pageNumber=${pageNumber}&${query}`
      );

      dispatch({
        type: GET_SETTINGS,
        payload: data,
      });
      dispatch({
        type: GET_SETTINGS_ENDED,
      });
    } catch (error) {
      dispatch({
        type: GET_SETTINGS_ENDED,
      });
      dispatch(handleErrorLocal(error));
      dispatch(handleError(error));
    }
  };
export const getSetting = (id) => async (dispatch) => {
  try {
    dispatch({
      type: GET_SETTING_STATED,
    });
    const { data } = await api.get(`/settings/${id}`);

    dispatch({
      type: GET_SETTING,
      payload: data,
    });
    dispatch({
      type: GET_SETTING_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_SETTING_STATED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const editSetting = (id, formData) => async (dispatch) => {
  try {
    dispatch({
      type: EDIT_SETTING_STATED,
    });
    const { data } = await api.put(`/settings/${id}`, formData);
    dispatch({
      type: EDIT_SETTING,
      payload: data,
    });
    dispatch({
      type: EDIT_SETTING_ENDED,
    });
  } catch (error) {
    dispatch({
      type: EDIT_SETTING_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const deleteSetting = (id) => async (dispatch) => {
  try {
    const { data } = await api.delete(`/settings/${id}`);
    dispatch(setAlert("Setting Deleted Successfully", "success"));
  } catch (error) {
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getAllSettings = ({ term, value }) => async (dispatch) => {
  try {
    dispatch({
      type: GET_ALL_SETTINGS_STATED,
    });
    const { data } = await api.get(`/settings/all?term=${term}&value=${value}`);

    dispatch({
      type: GET_ALL_SETTINGS,
      payload: data,
    });
    dispatch({
      type: GET_ALL_SETTINGS_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_ALL_SETTINGS_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};

export const handleErrorLocal = () => async (dispatch) => {};
