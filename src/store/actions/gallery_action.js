import api from "../../domain/api";
import {
  GET_GALLERYS_STATED,
  GET_GALLERYS,
  GET_GALLERYS_ENDED,
  ADD_GALLERY_STATED,
  ADD_GALLERY,
  ADD_GALLERY_ENDED,
  EDIT_GALLERY_STATED,
  EDIT_GALLERY,
  EDIT_GALLERY_ENDED,
  GET_GALLERY_STATED,
  GET_GALLERY,
  GET_GALLERY_ENDED,
  GET_ALL_GALLERYS_STATED,
  GET_ALL_GALLERYS,
  GET_ALL_GALLERYS_ENDED,
} from "../types/gallery_type";
import * as qs from "qs";
import { handleError } from "../../shared/handleError";
import { setAlert } from "./alert";

export const addGallery = (formData) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ADD_GALLERY_STATED,
    });
    const { data } = await api.post(`/gallerys`, formData);
    dispatch({
      type: ADD_GALLERY,
      payload: data,
    });
    dispatch({
      type: ADD_GALLERY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: ADD_GALLERY_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getGallerys =
  ({ pageNumber = ""}) =>
  async (dispatch) => {
    try {
      dispatch({
        type: GET_GALLERYS_STATED,
      });
      const queryParams = qs.parse(window.location.search.replace("?", ""));
      const query = qs.stringify(queryParams, {
        encodeValuesOnly: true, // prettify url
      });

      const { data } = await api.get(
        `/gallerys?&pageNumber=${pageNumber}&${query}`
      );

      dispatch({
        type: GET_GALLERYS,
        payload: data,
      });
      dispatch({
        type: GET_GALLERYS_ENDED,
      });
    } catch (error) {
      dispatch({
        type: GET_GALLERYS_ENDED,
      });
      dispatch(handleErrorLocal(error));
      dispatch(handleError(error));
    }
  };
export const getGallery = (id) => async (dispatch) => {
  try {
    dispatch({
      type: GET_GALLERY_STATED,
    });
    const { data } = await api.get(`/gallerys/${id}`);

    dispatch({
      type: GET_GALLERY,
      payload: data,
    });
    dispatch({
      type: GET_GALLERY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_GALLERY_STATED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const editGallery = (id, formData) => async (dispatch) => {
  try {
    dispatch({
      type: EDIT_GALLERY_STATED,
    });
    const { data } = await api.put(`/gallerys/${id}`, formData);
    dispatch({
      type: EDIT_GALLERY,
      payload: data,
    });
    dispatch({
      type: EDIT_GALLERY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: EDIT_GALLERY_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const deleteGallery = (id) => async (dispatch) => {
  try {
    const { data } = await api.delete(`/gallerys/${id}`);
    dispatch(setAlert("Gallery Deleted Successfully", "success"));
  } catch (error) {
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getAllGallerys = ({ term, value }) => async (dispatch) => {
  try {
    dispatch({
      type: GET_ALL_GALLERYS_STATED,
    });
    const { data } = await api.get(`/gallerys/all?term=${term}&value=${value}`);

    dispatch({
      type: GET_ALL_GALLERYS,
      payload: data,
    });
    dispatch({
      type: GET_ALL_GALLERYS_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_ALL_GALLERYS_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};

export const handleErrorLocal = () => async (dispatch) => {};
