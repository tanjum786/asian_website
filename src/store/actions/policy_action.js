import api from "../../domain/api";
import {
  GET_POLICYS_STATED,
  GET_POLICYS,
  GET_POLICYS_ENDED,
  ADD_POLICY_STATED,
  ADD_POLICY,
  ADD_POLICY_ENDED,
  EDIT_POLICY_STATED,
  EDIT_POLICY,
  EDIT_POLICY_ENDED,
  GET_POLICY_STATED,
  GET_POLICY,
  GET_POLICY_ENDED,
  GET_ALL_POLICYS_STATED,
  GET_ALL_POLICYS,
  GET_ALL_POLICYS_ENDED,
} from "../types/policy_type";
import * as qs from "qs";
import { handleError } from "../../shared/handleError";
import { setAlert } from "./alert";

export const addPolicy = (formData) => async (dispatch, getState) => {
  try {
    dispatch({
      type: ADD_POLICY_STATED,
    });
    const { data } = await api.post(`/policys`, formData);
    dispatch({
      type: ADD_POLICY,
      payload: data,
    });
    dispatch({
      type: ADD_POLICY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: ADD_POLICY_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getPolicys =
  ({ pageNumber = ""}) =>
  async (dispatch) => {
    try {
      dispatch({
        type: GET_POLICYS_STATED,
      });
      const queryParams = qs.parse(window.location.search.replace("?", ""));
      const query = qs.stringify(queryParams, {
        encodeValuesOnly: true, // prettify url
      });

      const { data } = await api.get(
        `/policys?&pageNumber=${pageNumber}&${query}`
      );

      dispatch({
        type: GET_POLICYS,
        payload: data,
      });
      dispatch({
        type: GET_POLICYS_ENDED,
      });
    } catch (error) {
      dispatch({
        type: GET_POLICYS_ENDED,
      });
      dispatch(handleErrorLocal(error));
      dispatch(handleError(error));
    }
  };
export const getPolicy = (id) => async (dispatch) => {
  try {
    dispatch({
      type: GET_POLICY_STATED,
    });
    const { data } = await api.get(`/policys/${id}`);

    dispatch({
      type: GET_POLICY,
      payload: data,
    });
    dispatch({
      type: GET_POLICY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_POLICY_STATED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const editPolicy = (id, formData) => async (dispatch) => {
  try {
    dispatch({
      type: EDIT_POLICY_STATED,
    });
    const { data } = await api.put(`/policys/${id}`, formData);
    dispatch({
      type: EDIT_POLICY,
      payload: data,
    });
    dispatch({
      type: EDIT_POLICY_ENDED,
    });
  } catch (error) {
    dispatch({
      type: EDIT_POLICY_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const deletePolicy = (id) => async (dispatch) => {
  try {
    const { data } = await api.delete(`/policys/${id}`);
    dispatch(setAlert("Policy Deleted Successfully", "success"));
  } catch (error) {
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};
export const getAllPolicys = ({ term, value }) => async (dispatch) => {
  try {
    dispatch({
      type: GET_ALL_POLICYS_STATED,
    });
    const { data } = await api.get(`/policys/all?term=${term}&value=${value}`);

    dispatch({
      type: GET_ALL_POLICYS,
      payload: data,
    });
    dispatch({
      type: GET_ALL_POLICYS_ENDED,
    });
  } catch (error) {
    dispatch({
      type: GET_ALL_POLICYS_ENDED,
    });
    dispatch(handleErrorLocal(error));
    dispatch(handleError(error));
  }
};

export const handleErrorLocal = () => async (dispatch) => {};
