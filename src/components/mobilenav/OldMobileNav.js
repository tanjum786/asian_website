import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useSelectAllCategory } from '../../shared/hooks/UseCategory'
import { useSelectAllService } from '../../shared/hooks/UseService'
import Loading from '../loading/Loading'
function Mobile_nav({ mobileMenuDrop }) {
    const [cat_data] = useSelectAllCategory()
    const { all_categorys, all_categorys_loading } = cat_data
    const [mainSpaCat, setMainSpaCat] = useState(null)
    const [mainSkinCat, setMainSkinCat] = useState(null)
    useEffect(() => {
        if (all_categorys) {
            const filteredSpaCat = all_categorys.filter(
                (item) => !item.parent_category && item.name == 'Spa & Salon Services',
            )
            setMainSpaCat(filteredSpaCat[0])
            const filteredSkinCat = all_categorys.filter(
                (item) => !item.parent_category && item.name == 'Skin Services',
            )
            setMainSkinCat(filteredSkinCat[0])
        }
    }, [all_categorys])
    const [activeSkinMenu, setActiveSkinMenu] = useState(null)
    const [activeSpaMenu, setActiveSpaMenu] = useState(null)
    const [subMenu, setSubMenu] = useState(null)
    const [mobileMenu, setMobileMenu] = useState(false)
    const [service_data] = useSelectAllService()
    const { all_services, all_services_loading } = service_data
    return (
        <div>
            <div className="" style={{ display: mobileMenuDrop ? 'block' : 'none' }}>
                <div className="mobile-header">
                    <div className="mobile-header-a">
                        <div className="desk-start">
                            <div className="mobile-logo">
                                <img src="/assets/images/logo.png"></img>
                            </div>
                            <hr></hr>
                            {!activeSkinMenu && !activeSpaMenu && (
                                <ul className="main-category">
                                    <li>
                                        <a className="dropdown-item" href="/">
                                            Home
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/about-us">About Us</a>
                                    </li>
                                    <li>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a onClick={() => setActiveSkinMenu(true)}>
                                                Medical Aesthetic Services
                                            </a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a onClick={() => setActiveSpaMenu(true)}>
                                                Spa Rituals and Wraps
                                            </a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/awards">Awards</a>
                                    </li>
                                    <li>
                                        <a href="/gallery">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="/blog">Blog</a>
                                    </li>
                                    <li>
                                        <a href="/contact-us">Contact us</a>
                                    </li>
                                </ul>
                            )}

                            {activeSkinMenu && !subMenu && (
                                <ul className="main-category" style={{ display: 'block' }}>
                                    <li>
                                        <a
                                            style={{
                                                padding: '5px 20px',
                                                background: 'rgb(255, 242, 242)',
                                                borderTop: 'none',
                                                borderRight: 'none',
                                                borderBottom: '1px solid rgb(255, 242, 242)',
                                                borderLeft: 'none',
                                                borderImage: 'initial',
                                            }}
                                            onClick={() => setActiveSkinMenu(false)}
                                        >
                                            <i className="fa fa-angle-left" /> Go Back to Main
                                        </a>
                                    </li>
                                    <li onClick={() => setSubMenu('Skin Services')}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a>Laser Hair Reduction</a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li onClick={() => setSubMenu('Face Sculpting')}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a>Body & Face Contouring & Sculpting</a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li onClick={() => setSubMenu('Anti-Aging Solutions')}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a>Anti-Aging Solutions</a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li onClick={() => setSubMenu('Skin Services')}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a>Skin Services</a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                    <li onClick={() => setSubMenu('Salon and Spa Services')}>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <a>Salon &amp; Spa Services</a>
                                            <button
                                                style={{
                                                    padding: '5px 20px',
                                                    background: 'rgb(255, 255, 255)',
                                                    borderTop: 'none',
                                                    borderRight: 'none',
                                                    borderBottom: '1px solid rgb(255, 242, 242)',
                                                    borderLeft: 'none',
                                                    borderImage: 'initial',
                                                }}
                                            >
                                                <i className="fa fa-angle-right" />
                                            </button>
                                        </div>
                                    </li>
                                </ul>
                            )}

                            {activeSpaMenu && (
                                <ul className="main-category" style={{ display: 'block' }}>
                                    <li>
                                        <a
                                            style={{
                                                padding: '5px 20px',

                                                borderTop: 'none',
                                                borderRight: 'none',
                                                borderBottom: '1px solid rgb(255, 242, 242)',
                                                borderLeft: 'none',
                                                borderImage: 'initial',
                                            }}
                                            onClick={() => setActiveSpaMenu(false)}
                                        >
                                            <i className="fa fa-angle-left" /> Go Back to Services
                                        </a>
                                    </li>

                                    <li>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <Link
                                                to={`/services/unisex-salon`}
                                                className="dropdown-item"
                                            >
                                                Unisex Salon
                                            </Link>
                                        </div>
                                    </li>

                                    <li>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <Link
                                                to={`/services/unisex-day-spa`}
                                                className="dropdown-item"
                                            >
                                                Unisex Day Spa
                                            </Link>
                                        </div>
                                    </li>
                                    <li>
                                        <div
                                            style={{
                                                display: 'flex',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <Link
                                                to={`/services/bridal-studio`}
                                                className="dropdown-item"
                                            >
                                                Bridal Studio
                                            </Link>
                                        </div>
                                    </li>
                                </ul>
                            )}

                            {activeSkinMenu && subMenu && (
                                <ul className="main-category" style={{ display: 'block' }}>
                                    <li>
                                        <a
                                            style={{
                                                padding: '5px 20px',

                                                borderTop: 'none',
                                                borderRight: 'none',
                                                borderBottom: '1px solid rgb(255, 242, 242)',
                                                borderLeft: 'none',
                                                borderImage: 'initial',
                                            }}
                                            onClick={() => {
                                                setActiveSpaMenu(false)
                                                setActiveSkinMenu(false)
                                                setSubMenu(null)
                                            }}
                                        >
                                            <i className="fa fa-angle-left" /> Go Back to Services
                                        </a>
                                    </li>
                                    {subMenu == 'Anti-Aging Solutions' && (
                                        <>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/micro-anti-ageing`}
                                                        className="dropdown-item"
                                                    >
                                                        Age Reversal
                                                    </Link>
                                                </div>
                                            </li>

                                            <li className="sub-hea">
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <h3>Non Invasive</h3>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/age-reversal`}
                                                        className="dropdown-item"
                                                    >
                                                        Exilis Elite
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/photofacial`}
                                                        className="dropdown-item"
                                                    >
                                                        Photofacial
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/thermage`}
                                                        className="dropdown-item"
                                                    >
                                                        Thermage
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/hifu`}
                                                        className="dropdown-item"
                                                    >
                                                        HIFU
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/ultherapy`}
                                                        className="dropdown-item"
                                                    >
                                                        Ultherapy
                                                    </Link>
                                                </div>
                                            </li>
                                            <li className="sub-hea">
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <h3>Minimal Invasive</h3>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    {/* <Link
                                                        to={`/services/botox`}
                                                        className="dropdown-item"
                                                    >
                                                        Botox
                                                    </Link> */}
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/fillers`}
                                                        className="dropdown-item"
                                                    >
                                                        Fillers
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/thread-lifts`}
                                                        className="dropdown-item"
                                                    >
                                                        Thread Lifts
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/skin-boosters`}
                                                        className="dropdown-item"
                                                    >
                                                        PRP and Skin Boosters Etc
                                                    </Link>
                                                </div>
                                            </li>
                                        </>
                                    )}
                                    {subMenu == 'Skin Services' && (
                                        <>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/photofacial`}
                                                        className="dropdown-item"
                                                    >
                                                        Power Glow facials
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/oxyfacial-oxygeneo-facials`}
                                                        className="dropdown-item"
                                                    >
                                                        Oxyfacial
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/servicesoxyfacial-facials`}
                                                        className="dropdown-item"
                                                    >
                                                        OxyGeneo Facial
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/hydrafacial`}
                                                        className="dropdown-item"
                                                    >
                                                        Hydrafacial
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/chemical-peels`}
                                                        className="dropdown-item"
                                                    >
                                                        Chemical Peels
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/unisex-day-spa`}
                                                        className="dropdown-item"
                                                    >
                                                        Inara by Forrest Essential Range of
                                                        Facials
                                                    </Link>
                                                </div>
                                            </li>
                                            <li>
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <Link
                                                        to={`/services/unisex-day-spa`}
                                                        className="dropdown-item"
                                                    >
                                                        The Luxurious Thalgo Facials
                                                    </Link>
                                                </div>
                                            </li>
                                        </>
                                    )}

                                    {subMenu == 'Salon and Spa Services' && (
                                        <>
                                            <li>
                                                <Link to="/services/unisex-salon" className="dropdown-item" >

                                                    Hair and Scalp Rituals and Treatments
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/services/unisex-day-spa" className="dropdown-item" >

                                                    Body Wraps and Rituals
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/services/unisex-salon" className="dropdown-item" >

                                                    Nail Bar
                                                </Link>

                                            </li>
                                        </>
                                    )}
                                    {subMenu == 'Face Sculpting' && (
                                        <>
                                            <li>
                                                <Link to="/services/facial-contouring" className="dropdown-item" >

                                                    Body & Face Contouring & Sculpting
                                                </Link>
                                            </li>

                                        </>
                                    )}
                                </ul>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Mobile_nav
