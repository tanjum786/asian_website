import { useState, useEffect,useCallback } from "react";
import { useSelector, useDispatch  } from "react-redux";
import {
  addSetting,
  getSettings,
  getSetting,
  editSetting,
  deleteSetting,
  getAllSettings,
} from "../../store/actions/setting_action";
import _debounce from "lodash/debounce";
// import { useSelectAllIndustry } from "./UseIndustry";

// Get All Data
export const useAllSettings = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.setting);
  const [pageNumber, setPageNumber] = useState(1);
  const [deleteEntry, setDeleteEntry] = useState(null);
 
  useEffect(() => {
    if (deleteEntry) {
      dispatch(deleteSetting(deleteEntry));
  }
    allQuery();
  }, [deleteEntry, pageNumber, window.location.search]);
  const allQuery = useCallback(
    _debounce(() => {
     
      dispatch(
        getSettings({
          pageNumber,
        })
      );
    }, 1000),
    []
  );

  useEffect(() => {
    setPageNumber(1);
  }, [window.location.search]);

  const deleteBtnClicked = async (id) => {
    setDeleteEntry(id);
  };

  return [
    data, setPageNumber, deleteBtnClicked
  ];
};

// Get Single Data
export const useSingleSetting = (id) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.setting);
  useEffect(() => {
    dispatch(getSetting(id));
  }, [id]);
  return [data];
};
// Add Data
export const useCreateSetting = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.setting);
  const addData = async (data) => {
    await dispatch(addSetting(data));
  };
  return [data, addData];
};
export const useUpdateSetting = () => {
  const dispatch = useDispatch();
  // const data = useSelector((state) => state.setting);
  const updateData = async (id, data) => {
    await dispatch(editSetting(id, data));
  };
  return [updateData];
};

export const useSelectAllSetting = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState("");
  const [value, setValue] = useState("");
  const data = useSelector((state) => state.setting);
  useEffect(() => {
    dispatch(getAllSettings({ term, value }));
  }, [term, value]);
  return [data, setTerm, setValue];
};


export const useGetDropdownOptions = () => {
  //  const [client, setClientSearchField, setClientSearchValue] =
    // useSelectAllClient();
 
  const [dropdownOptions, setDropdownOptions] = useState({});
  // useEffect(() => {
  //   if (industry && industry.all_industrys) {
  //     const newData = industry.all_industrys.map((item) => {
  //       return { label: item.name, value: item.name };
  //     });
  //     setDropdownOptions({ ...dropdownOptions, industry: newData });
  //   }
  // }, [industry]);
  const loadOptions = async (inputValue, callback, field) => {
    // if (field == "parent_category") {
    //   await setCategorySearchField("name");
    //   await setCategorySearchValue(inputValue);
    //   callback(dropdownOptions.parent_category);
    // }
  };

 
  return [dropdownOptions,loadOptions  ];
};