import { useState, useEffect,useCallback } from "react";
import { useSelector, useDispatch  } from "react-redux";
import {
  addPolicy,
  getPolicys,
  getPolicy,
  editPolicy,
  deletePolicy,
  getAllPolicys,
} from "../../store/actions/policy_action";
import _debounce from "lodash/debounce";
// import { useSelectAllIndustry } from "./UseIndustry";

// Get All Data
export const useAllPolicys = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.policy);
  const [pageNumber, setPageNumber] = useState(1);
  const [deleteEntry, setDeleteEntry] = useState(null);
 
  useEffect(() => {
    if (deleteEntry) {
      dispatch(deletePolicy(deleteEntry));
  }
    allQuery();
  }, [deleteEntry, pageNumber, window.location.search]);
  const allQuery = useCallback(
    _debounce(() => {
     
      dispatch(
        getPolicys({
          pageNumber,
        })
      );
    }, 1000),
    []
  );

  useEffect(() => {
    setPageNumber(1);
  }, [window.location.search]);

  const deleteBtnClicked = async (id) => {
    setDeleteEntry(id);
  };

  return [
    data, setPageNumber, deleteBtnClicked
  ];
};

// Get Single Data
export const useSinglePolicy = (id) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.policy);
  useEffect(() => {
    dispatch(getPolicy(id));
  }, [id]);
  return [data];
};
// Add Data
export const useCreatePolicy = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.policy);
  const addData = async (data) => {
    await dispatch(addPolicy(data));
  };
  return [data, addData];
};
export const useUpdatePolicy = () => {
  const dispatch = useDispatch();
  // const data = useSelector((state) => state.policy);
  const updateData = async (id, data) => {
    await dispatch(editPolicy(id, data));
  };
  return [updateData];
};

export const useSelectAllPolicy = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState("");
  const [value, setValue] = useState("");
  const data = useSelector((state) => state.policy);
  useEffect(() => {
    dispatch(getAllPolicys({ term, value }));
  }, [term, value]);
  return [data, setTerm, setValue];
};


export const useGetDropdownOptions = () => {
  //  const [client, setClientSearchField, setClientSearchValue] =
    // useSelectAllClient();
 
  const [dropdownOptions, setDropdownOptions] = useState({});
  // useEffect(() => {
  //   if (industry && industry.all_industrys) {
  //     const newData = industry.all_industrys.map((item) => {
  //       return { label: item.name, value: item.name };
  //     });
  //     setDropdownOptions({ ...dropdownOptions, industry: newData });
  //   }
  // }, [industry]);
  const loadOptions = async (inputValue, callback, field) => {
    // if (field == "parent_category") {
    //   await setCategorySearchField("name");
    //   await setCategorySearchValue(inputValue);
    //   callback(dropdownOptions.parent_category);
    // }
  };

 
  return [dropdownOptions,loadOptions  ];
};