import { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  addAppointment,
  getAppointments,
  getAppointment,
  editAppointment,
  deleteAppointment,
  getAllAppointments,
} from "../../store/actions/appointment_action";
import _debounce from "lodash/debounce";
import { useSelectAllService } from "./UseService";

// Get All Data
export const useAllAppointments = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.appointment);
  const [pageNumber, setPageNumber] = useState(1);
  const [deleteEntry, setDeleteEntry] = useState(null);

  useEffect(() => {
    if (deleteEntry) {
      dispatch(deleteAppointment(deleteEntry));
    }
    allQuery();
  }, [deleteEntry, pageNumber, window.location.search]);
  const allQuery = useCallback(
    _debounce(() => {
      dispatch(
        getAppointments({
          pageNumber,
        })
      );
    }, 1000),
    []
  );

  useEffect(() => {
    setPageNumber(1);
  }, [window.location.search]);

  const deleteBtnClicked = async (id) => {
    setDeleteEntry(id);
  };

  return [data, setPageNumber, deleteBtnClicked];
};

// Get Single Data
export const useSingleAppointment = (id) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.appointment);
  useEffect(() => {
    dispatch(getAppointment(id));
  }, [id]);
  return [data];
};
// Add Data
export const useCreateAppointment = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.appointment);
  const addData = async (data) => {
    await dispatch(addAppointment(data));
  };
  return [data, addData];
};
export const useUpdateAppointment = () => {
  const dispatch = useDispatch();
  // const data = useSelector((state) => state.appointment);
  const updateData = async (id, data) => {
    await dispatch(editAppointment(id, data));
  };
  return [updateData];
};

export const useSelectAllAppointment = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState("");
  const [value, setValue] = useState("");
  const data = useSelector((state) => state.appointment);
  useEffect(() => {
    dispatch(getAllAppointments({ term, value }));
  }, [term, value]);
  return [data, setTerm, setValue];
};

export const useGetDropdownOptions = () => {
  const [service, setServiceSearchField, setServiceSearchValue] =
    useSelectAllService();

  const [dropdownOptions, setDropdownOptions] = useState({});
  useEffect(() => {
    if (service && service.all_services) {
      const newData = service.all_services.map((item) => {
        return { label: item.name, value: item._id };
      });
      setDropdownOptions({ ...dropdownOptions, service: newData });
    }
  }, [service]);
  const loadOptions = async (inputValue, callback, field) => {
    if (field == "service") {
      await setServiceSearchField("name");
      await setServiceSearchValue(inputValue);
      callback(dropdownOptions.parent_category);
    }
  };

  return [dropdownOptions, loadOptions];
};
