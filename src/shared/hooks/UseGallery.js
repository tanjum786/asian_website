import { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  addGallery,
  getGallerys,
  getGallery,
  editGallery,
  deleteGallery,
  getAllGallerys,
} from "../../store/actions/gallery_action";
import _debounce from "lodash/debounce";
import { useSelectAllService } from "./UseService";

// Get All Data
export const useAllGallerys = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.gallery);
  const [pageNumber, setPageNumber] = useState(1);
  const [deleteEntry, setDeleteEntry] = useState(null);

  useEffect(() => {
    if (deleteEntry) {
      dispatch(deleteGallery(deleteEntry));
    }
    allQuery();
  }, [deleteEntry, pageNumber, window.location.search]);
  const allQuery = useCallback(
    _debounce(() => {
      dispatch(
        getGallerys({
          pageNumber,
        })
      );
    }, 1000),
    []
  );

  useEffect(() => {
    setPageNumber(1);
  }, [window.location.search]);

  const deleteBtnClicked = async (id) => {
    setDeleteEntry(id);
  };

  return [data, setPageNumber, deleteBtnClicked];
};

// Get Single Data
export const useSingleGallery = (id) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.gallery);
  useEffect(() => {
    dispatch(getGallery(id));
  }, [id]);
  return [data];
};
// Add Data
export const useCreateGallery = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.gallery);
  const addData = async (data) => {
    await dispatch(addGallery(data));
  };
  return [data, addData];
};
export const useUpdateGallery = () => {
  const dispatch = useDispatch();
  // const data = useSelector((state) => state.gallery);
  const updateData = async (id, data) => {
    await dispatch(editGallery(id, data));
  };
  return [updateData];
};

export const useSelectAllGallery = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState("");
  const [value, setValue] = useState("");
  const data = useSelector((state) => state.gallery);
  useEffect(() => {
    dispatch(getAllGallerys({ term, value }));
  }, [term, value]);
  return [data, setTerm, setValue];
};

export const useGetDropdownOptions = () => {
  const [service, setServiceSearchField, setServiceSearchValue] =
    useSelectAllService();

  const [dropdownOptions, setDropdownOptions] = useState({});
  useEffect(() => {
    if (service && service.all_services) {
      const newData = service.all_services.map((item) => {
        return { label: item.name, value: item._id };
      });
      setDropdownOptions({ ...dropdownOptions, service: newData });
    }
  }, [service]);
  const loadOptions = async (inputValue, callback, field) => {
    if (field == "service") {
      await setServiceSearchField("name");
      await setServiceSearchValue(inputValue);
      callback(dropdownOptions.parent_category);
    }
  };

  return [dropdownOptions, loadOptions];
};
