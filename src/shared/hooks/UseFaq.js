import { useState, useEffect,useCallback } from "react";
import { useSelector, useDispatch  } from "react-redux";
import {
  addFaq,
  getFaqs,
  getFaq,
  editFaq,
  deleteFaq,
  getAllFaqs,
} from "../../store/actions/faq_action";
import _debounce from "lodash/debounce";
// import { useSelectAllIndustry } from "./UseIndustry";

// Get All Data
export const useAllFaqs = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.faq);
  const [pageNumber, setPageNumber] = useState(1);
  const [deleteEntry, setDeleteEntry] = useState(null);
 
  useEffect(() => {
    if (deleteEntry) {
      dispatch(deleteFaq(deleteEntry));
  }
    allQuery();
  }, [deleteEntry, pageNumber, window.location.search]);
  const allQuery = useCallback(
    _debounce(() => {
     
      dispatch(
        getFaqs({
          pageNumber,
        })
      );
    }, 1000),
    []
  );

  useEffect(() => {
    setPageNumber(1);
  }, [window.location.search]);

  const deleteBtnClicked = async (id) => {
    setDeleteEntry(id);
  };

  return [
    data, setPageNumber, deleteBtnClicked
  ];
};

// Get Single Data
export const useSingleFaq = (id) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.faq);
  useEffect(() => {
    dispatch(getFaq(id));
  }, [id]);
  return [data];
};
// Add Data
export const useCreateFaq = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.faq);
  const addData = async (data) => {
    await dispatch(addFaq(data));
  };
  return [data, addData];
};
export const useUpdateFaq = () => {
  const dispatch = useDispatch();
  // const data = useSelector((state) => state.faq);
  const updateData = async (id, data) => {
    await dispatch(editFaq(id, data));
  };
  return [updateData];
};

export const useSelectAllFaq = () => {
  const dispatch = useDispatch();
  const [term, setTerm] = useState("");
  const [value, setValue] = useState("");
  const data = useSelector((state) => state.faq);
  useEffect(() => {
    dispatch(getAllFaqs({ term, value }));
  }, [term, value]);
  return [data, setTerm, setValue];
};


export const useGetDropdownOptions = () => {
  //  const [client, setClientSearchField, setClientSearchValue] =
    // useSelectAllClient();
 
  const [dropdownOptions, setDropdownOptions] = useState({});
  // useEffect(() => {
  //   if (industry && industry.all_industrys) {
  //     const newData = industry.all_industrys.map((item) => {
  //       return { label: item.name, value: item.name };
  //     });
  //     setDropdownOptions({ ...dropdownOptions, industry: newData });
  //   }
  // }, [industry]);
  const loadOptions = async (inputValue, callback, field) => {
    // if (field == "parent_category") {
    //   await setCategorySearchField("name");
    //   await setCategorySearchValue(inputValue);
    //   callback(dropdownOptions.parent_category);
    // }
  };

 
  return [dropdownOptions,loadOptions  ];
};